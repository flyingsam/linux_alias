alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias df='df -h'
alias du='du -h'
alias less='less -r'                          # raw control characters
alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
alias ls='ls -hF --color=tty'                 # classify files in colour
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
alias l='ls -lrt'                              # long list
alias la='ls -Al'                              # all but . and ..
alias s='cd ..' 	# up one dir
alias cdd='cd -' 	# goto last dir cd'ed from
alias lt='ls -lt | more'	# sort with recently modified first
alias h='history'
alias c='clear'
alias cl='clear; l'
alias cls='clear; ls'
alias e='exit'
alias sup='svn update'
alias supety='svn update --depth empty'
alias sls='svn list'
alias ss='svn status '
alias sd='svn diff '
alias scom='svn commit'
alias slog='svn log | less'
alias sex='svn export'

# reload bash aliases
alias rld="source ~/.bashrc"

# git helpers
alias gaa="git add -A"
alias gu="git pull origin master"
alias gp="git push origin master"
alias ga="git add ."
alias gc="git commit -m \$1"
alias gs="git status"
alias gi="nano .gitignore"
alias grv="git remote -v"
alias gd="git diff"

# git config (globally)
alias ggmyname="git config --global user.name \$1"
alias ggmyemail="git config --global user.email \$1"

# git config (locally)
alias gmyname="git config user.name \$1"
alias gmyemail="git config user.email \$1"

# disk space and cls/clear
alias left="df -h"
alias cls="clear"

alias vi='vim'
alias lf='ls `pwd`/*'
alias ubuntu='echo '123' |xsel && ssh 192.168.128.130 -l sam'
alias fedora='echo '123' |xsel && ssh 192.168.128.131 -l sam'
alias centos='echo '123' |xsel && ssh 192.168.128.129 -l sam'
alias ecopkf='echo 'dzqd!QA21' |xsel && ssh root@172.16.1.216'
alias abckf='echo '123456' |xsel && ssh oppf@172.16.1.220'
alias svnsev='echo 'sonarcrm' |xsel && ssh sonarcrm@172.16.1.200'
alias s106='ssh 192.168.1.106 -l sam'
alias cdmvno='cd /cygdrive/e/HXLX/02代码/java\(402\)/MVNO'
alias ngcrm='cd /cygdrive/d/项目/02代码/03CRM/NGCRM/trunk/NGCRM'
alias joc='cd /cygdrive/d/项目/02代码/03CRM/CRM_Products/jcoc'
alias boss1.5='cd /cygdrive/d/项目/02代码/03CRM/NGCRM/trunk/BOSS1.5'
alias s226='echo Mvno@12 && ssh -l lcs 192.168.32.226'
alias mvn='mvn -DskipTests=true'
alias o='less'
alias m='most'
alias s242='ssh -l mvno 192.168.32.242'
alias kf1='mysql -u root -proot -h 192.168.32.242 -P 5383'
alias kf2='mysql -u root -proot -h 192.168.32.242 -P 5393'
alias mjoc='mysql -u root -p123456 -h172.16.1.170 -P 3306'
alias mvnotags="cd /home/sam/project/MVNO && ctags --exclude='*/target/*' --exclude='*mvno.crm/src/main/webapp/extjs/*' --exclude='*mvno.crm/src/main/webapp/mvnocrm/callcenter/webseat/*' --exclude='*mvno.interface/src/test/web/js/*' -R *"
alias tcatlog='tail -f ~/app/apache-tomcat-7.0.55/logs/catalina.out'
#alias ngkfh='echo lcs123 |xsel && ssh lcs@172.16.1.110'
alias ngkfh='echo abc123 |xsel && ssh lcs@172.16.1.112'
alias daily='cd /cygdrive/d/CRM研发部/08小组工作区/02营销组/01小组日报/营销组小组日报`date +%Y%m`'
# env var
# export MAVEN_OPTS="-XX:+CMSClassUnloadingEnabled -XX:PermSize=256M -XX:MaxPermSize=512M -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"
export MAVEN_OPTS="-XX:+CMSClassUnloadingEnabled -XX:PermSize=256M -XX:MaxPermSize=512M -Djava.compiler=NONE -Duser.language=en"
# No ttyctl, so we need to save and then restore terminal settings
stty ixany
stty ixoff -ixon
# ngcrm oracle database
alias ngfsdb='cd /home/sam/project/ngcrm_quicksql ; sqlplus NGCRM_FS/Gmcc123@yxcrm'
alias ngcomdb='sqlplus NGCRM_COMM/Gmcc123@yxcrm'
alias ngupldpath='cd /cygdrive/d/项目/01文档/06上载目录/03CRM/未上载'
alias jboss='cd /cygdrive/d/jboss-4.2.3.GA'
alias cics='cd /cygdrive/d/项目/02代码/03CRM/NGCRM/trunk/C++/cics'
alias temp='cd /cygdrive/d/temp'
alias javac='javac -J-Duser.language=en'
# alias fsuat='echo Gmcc@123 |xsel && ssh lisl@192.168.211.251'
alias fsuat='echo Gmcc@123 |xsel && ssh lisl@172.16.1.20'
alias ttygbk=' mintty -o charset=gbk -w max &'
alias wcd='source wcd'
alias ngdesign='cd /cygdrive/d/项目/01文档/03设计'
alias goagent='nohup python /home/sam/app/goagent/local/proxy.py > ~/logs/goagent.log &'
alias ngsqltpl="cd '/cygdrive/c/Program Files (x86)/PLSQL Developer/Template/NGCRM'"
alias unitt='cd  /cygdrive/d/项目/01文档/03设计/05单元测试/版本需求单元测试/'
# set -o vi
shopt -s nocaseglob
alias bugdesc='cd /cygdrive/d/项目/01文档/03设计/07BUG说明书/CRM研发部/`date +%Y%m`'
alias bugreg='cd /cygdrive/d/项目/01文档/02需求/02版本相关/10\ CRM开发计划'
alias sqljudge='cd /cygdrive/d/项目/01文档/03设计/08程序内SQL评审/CRM/14.9.0'
alias uattestreport='cd /cygdrive/d/项目/01文档/04测试/02测试报告/01CRM'
alias uattestcase='cd /cygdrive/d/项目/01文档/04测试/01测试用例/01CRM'
# export ANDROID_EMULATOR_FORCE_32BIT=true
[[ -s /home/sam/.autojump/etc/profile.d/autojump.sh ]] && source /home/sam/.autojump/etc/profile.d/autojump.sh
export EDITOR=VIM
export PAGER=most
#export PAGER=less
alias nng168="echo 'abc123' |xsel && ssh root@172.16.1.168"
alias nng169="echo 'abc123' |xsel && ssh root@172.16.1.169"
alias nng170="echo 'abc123' |xsel && ssh root@172.16.1.170"
alias aiesb1="echo 'abc123' |xsel && ssh root@172.16.1.160"
alias aiesb2="echo 'abc123' |xsel && ssh root@172.16.1.161"
alias aiesb3="echo 'abc123' |xsel && ssh root@172.16.1.162"
alias flume1="echo 'hadoop' |xsel && ssh hadoop@192.168.35.197"
alias flume2="echo 'hadoop' |xsel && ssh hadoop@192.168.35.198"
alias flume3="echo 'hadoop' |xsel && ssh hadoop@192.168.35.199"
alias hadoop1="echo 'hadoop' |xsel && ssh hadoop@172.16.1.181"
alias hadoop2="echo 'hadoop' |xsel && ssh hadoop@172.16.1.182"
alias hadoop3="echo 'hadoop' |xsel && ssh hadoop@172.16.1.183"
alias idea="xpl '/cygdrive/d/Program Files (x86)/JetBrains/IntelliJ IDEA 13.1.4/bin/idea.exe'"

# function
function sr {
    find . -type f -exec sed -i s/$1/$2/g {} +
}
function col {
    awk -v col=$1 '{print $col}'
}
function skip {
    n=$(($1 + 1))
    cut -d' ' -f$n-
}
function dur {
  case $1 in
  clone|cl)
    git clone git@bitbucket.org:nicolapaolucci/$2.git
    ;;
  move|mv)
    git remote add bitbucket git@bitbucket.org:nicolapaolucci/$(basename $(pwd)).git
    git push --all bitbucket
    ;;
  trackall|tr)
    #track all remote branches of a project
    for remote in $(git branch -r | grep -v master ); do git checkout --track $remote ; done
    ;;
  key|k)
    #track all remote branches of a project
    ssh $2 'mkdir -p .ssh && cat >> .ssh/authorized_keys' < ~/.ssh/id_rsa.pub
    ;;
  fun|f)
    #list all custom bash functions defined
    typeset -F | col 3 | grep -v _ | xargs | fold -sw 60
    ;;
  def|d)
    #show definition of function $1
    typeset -f $2
    ;;
  help|h|*)
    echo "[dur]dn shell automation tools"
    echo "commands available:"
    echo " [cl]one, [mv|move]"
    echo " [f]fun lists all bash functions defined in .bashrc"
    echo " [def] <fun> lists definition of function defined in .bashrc"
    echo " [k]ey <host> copies ssh key to target host"
    echo " [tr]ackall], [h]elp"
    ;;
  esac
}
mcd() { mkdir -p "$1"; cd "$1";}
backup() { cp "$1"{,.bak};}
md5check() { md5sum "$1" | grep "$2";}
#很容易用你上一个运行的命令创建一个脚本：makescript [脚本名字.sh]
alias makescript="fc -rnl | head -1 >"
alias genpasswd="strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 30 | tr -d '\n'; echo"
#很长，但是也是最有用的。解压任何的文档类型：extract: [压缩文件]
extract() { 
    if [ -f $1 ] ; then 
      case $1 in 
        *.tar.bz2)   tar xjf $1     ;; 
        *.tar.gz)    tar xzf $1     ;; 
        *.bz2)       bunzip2 $1     ;; 
        *.rar)       unrar e $1     ;; 
        *.gz)        gunzip $1      ;; 
        *.tar)       tar xf $1      ;; 
        *.tbz2)      tar xjf $1     ;; 
        *.tgz)       tar xzf $1     ;; 
        *.zip)       unzip $1       ;; 
        *.Z)         uncompress $1  ;; 
        *.7z)        7z x $1        ;; 
        *)     echo "'$1' cannot be extracted via extract()" ;; 
         esac 
     else 
         echo "'$1' is not a valid file" 
     fi 
}
alias cmount="mount | column -t"
#以树形结构递归地显示目录结构。
alias tree="ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'"
#安装文件在磁盘存储的大小排序，显示当前目录的文件列表
sbs() { du -b --max-depth 1 | sort -nr | perl -pe 's{([0-9]+)}{sprintf "%.1f%s", $1>=2**30? ($1/2**30, "G"): $1>=2**20? ($1/2**20, "M"): $1>=2**10? ($1/2**10, "K"): ($1, "")}e';}
#接管某个进程的标准输出和标准错误。注意你需要安装了 strace。
alias intercept="sudo strace -ff -e trace=write -e write=1,2 -p"
#查看你还有剩下多少内存。
alias meminfo='free -m -l -t'
alias ps?="ps aux | grep"
#下载整个网站：websiteget [URL]。
alias websiteget="wget --random-wait -r -p -e robots=off -U mozilla"
#显示出哪个应用程序连接到网络。
alias listen="lsof -P -i -n"
#显示出活动的端口。
alias port='netstat -tulanp'
#获得你的公网IP地址和主机名。
alias ipinfo="curl ifconfig.me && curl ifconfig.me/host"
#返回你的当前IP地址的地理位置。
getlocation() { lynx -dump http://www.ip-adress.com/ip_tracer/?QRY=$1|grep address|egrep 'city|state|country'|awk '{print $3,$4,$5,$6,$7,$8}'|sed 's\ip address flag \\'|sed 's\My\\';}
#绘制内核模块依赖曲线图。需要可以查看图片。
kernelgraph() { lsmod | perl -e 'print "digraph \"lsmod\" {";<>;while(<>){@_=split/\s+/; print "\"$_[0]\" -> \"$_\"\n" for split/,/,$_[3]}print "}"' | dot -Tpng | display -;}
#在那些非技术人员的眼里你看起来是总是那么忙和神秘。
alias busy="cat /dev/urandom | hexdump -C | grep 'ca fe'"

#H2database
alias h2db='java -jar "$HOMEPATH\.m2\repository\com\h2database\h2\1.4.184\h2-1.4.184.jar"'
alias luke='java -jar $LUKE_HOME/lukeall.jar &'
#Get your external IP address 
alias ipall='curl http://ifconfig.co/all.json'
#Making scripts runs on backgourd and logging output
alias bgr="nohup bash \$1 2>&1 | tee -i i-like-log-files.log &"
#Getting a domain from url, ex: very nice to get url from squid access.log
alias dm='sed -e "s/[^/]*\/\/\([^@]*@\)\?\([^:/]*\).*/\2/"'
#list files recursively by size
alias lfs="stat -c'%s %n' **/* | sort -n"
alias b2d='boot2docker'
export OMP='/cygdrive/d/NG3.0channel/01项目代码/java/NG3_QD_Trunk/ngecop/omp'
alias swbromp="swbr 'ngecop/omp'  $OMP"
function swdevomp {
    swdev 03ngecop/$1/omp $OMP
}
function smbromp {
    smbr 03ngecop/$1/omp $2 $OMP
}
export ECOUPON='/cygdrive/d/NG3.0channel/01项目代码/java/NG3_QD_Trunk/ngecop/ecop-ecoupon'
alias swbrecoupon="swbr 'ngecop/ecop-ecoupon'  $ECOUPON"
function swdevecoupon {
    swdev 03ngecop/$1/ecop-ecoupon $ECOUPON
}
function smbrecoupon {
    smbr 03ngecop/$1/ecop-ecoupon $2 $ECOUPON
}
export WAP="/cygdrive/d/NG3.0channel/01项目代码/java/NG3_QD_Trunk/ngecop/ecop-wap"
alias swbrwap="swbr 'ngecop/ecop-wap'  $WAP"
function swdevwap {
    swdev 03ngecop/$1/client-wap $WAP/client-wap
}

function smbrwap {
    smbr 03ngecop/$1/client-wap $2 $WAP/client-wap
}
export OLDCOMMODITY="/cygdrive/d/NG3.0channel/01项目代码/java/NG3_QD_Trunk/ngecop/ecop-commodity/ECOP_Commodity"
alias swbrocom="swbr 'ngecop/ecop-commodity/ECOP_Commodity'  $OLDCOMMODITY"
function swdevocom {
    swdev 03ngecop/$1/ecop-commodity/ECOP_Commodity $OLDCOMMODITY
}

export ECOUPONRELEASE='/cygdrive/d/NG3.0channel/01项目代码/java/NG3_QD_Trunk/ngecop/daemon/deamon-ECouponRelease'
alias swbrecouponrelease="swbr 'ngecop/daemon/deamon-ECouponRelease'  $ECOUPONRELEASE"
function swdevecouponrelease {
    swdev 03ngecop/$1/deamon-ECouponRelease $ECOUPONRELEASE
}
function smbrecouponrelease {
    smbr 03ngecop/$1/deamon-ECouponRelease $2 $ECOUPONRELEASE
}


alias startzk="xpl /home/sam/app/zookeeper-3.4.6/bin/zkServer.cmd"
